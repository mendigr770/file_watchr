﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace events
{

    class DirectoryWatcher
    {
        private string searchPattern;
        private string path;
        private Func<string, string[]> GetFiles;
        private Dictionary<string, DateTime> files;
        public event EventHandler DirectoryChanged;
        public DirectoryWatcher(string path, Func<string, string[]> GetFiles)
        {
            this.path = path;
            this.GetFiles = GetFiles;
            DirectoryChanged += AlertWasChangeMade;
            files = GetFilesToDict(path, GetFiles)
                .ToDictionary(entry => entry.Key, entry => entry.Value);
        }

        public void WatchOnDirectory()
        {
            Dictionary<string, DateTime> currFiles; 
            while (this.files.Count != 0)
            {
                
                currFiles = GetFilesToDict(this.path, this.GetFiles);
                if (currFiles.Count != this.files.Count || currFiles.Except(this.files).Any())
                {
                    DirectoryChanged?.Invoke(this, new EventArgs());
                    files = GetFilesToDict(this.path, this.GetFiles);
                }
            }
        }

        public void AlertWasChangeMade(Object sender, EventArgs e)
        {
            Console.WriteLine("Heyyyyy!! Somone changed our files. be carefull");
        }

        public Dictionary<string, DateTime> GetFilesToDict(string path,
            Func<string, string[]> GetFiles)
        {
            Dictionary<string, DateTime> newFilesDict = new Dictionary<string, DateTime>();
            try
            {
                string[] filePaths = GetFiles(path);

                foreach (string filePath in filePaths)
                {
                    DateTime lastWriteTime = File.GetLastWriteTime(filePath);
                    newFilesDict.Add(filePath, lastWriteTime);
                }
                if (filePaths.Length == 0)
                {
                    Console.WriteLine("No files found in this directry");
                }
            }
            catch (DirectoryNotFoundException e)
            {
                Console.WriteLine(e.Message);
            }
            catch (ArgumentException e)
            {
                Console.WriteLine(e.Message);
            }

            return newFilesDict;
        }
    }
}
