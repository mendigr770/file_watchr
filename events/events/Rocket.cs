﻿using System;
using System.Collections.Generic;

namespace events
{

    class Rocket
    {
        //Input: Collection and list of functions
        //Behavior: For each item in the collection, choose function from the list at
        //random and run on the item.
        public void second<T>(List<T> words, List<Action<T>> actions)
        {
            Random rd = new Random();
            int rand_num;
            foreach (T word in words)
            {
                rand_num = rd.Next(actions.Count);
                actions[rand_num](word);
            }
        }
    }
}
