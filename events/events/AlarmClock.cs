﻿using System;

namespace events
{

    class AlarmClock
    {
        //Inputs: Date-Time object and a Function.
        //Behavior: Execute the function at given time.
        public void CheckTime(DateTime time, Action Ring)
        {
            while (DateTime.Now < time) { }
            Ring();
        }
    }
}
