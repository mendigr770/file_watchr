﻿using System;
using System.Collections.Generic;
using System.IO;

namespace events
{
    class Program
    {
        static void Main(string[] args)
        {
            //2
            //AlarmClock a = new AlarmClock();
            //a.CheckTime(DateTime.Now, Ring);

            //3
            //Rocket rocket = new Rocket();
            //List<string> strs = new List<string> { "aaa", "bbb" };
            //List<Action<String>> functions = new List<Action<String>>
            //{FirstRring, SecondRing, TherdRing, FourRing};
            //rocket.second<string>(strs, functions);

            //1
            string PATH = "C:\\Users\\noamiko\\Desktop\\mendy greenwald\\test";
            DirectoryWatcher fileWatcher = new DirectoryWatcher(PATH, GetFilesPath);
            fileWatcher.WatchOnDirectory();
            Console.Read();
        }

        static void Ring()
        {
            Console.WriteLine("aaaaaaaaaaaaaaaaa");
        }
        static void FirstRring(string str)
        {
            Console.WriteLine($"first funtion was executed with {str}");
        }
        static void SecondRing(string str)
        {
            Console.WriteLine($"second funtion was executed with {str}");
        }
        static void TherdRing(string str)
        {
            Console.WriteLine($"therd funtion was executed with {str}");
        }
        static void FourRing(string str)
        {
            Console.WriteLine($"four funtion was executed with {str}");
        }

        static string[] GetFilesPath(string path)
        { return Directory.GetFiles(@path, "*.txt"); }
    }
}
